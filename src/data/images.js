// Content
import '../assets/img/content/project-1.jpg'
import '../assets/img/content/project-2.jpg'
import '../assets/img/content/project-3.jpg'
import '../assets/img/content/project-4.jpg'


// Icons
import '../assets/img/behance-light.svg'
import '../assets/img/facebook-light.svg'
import '../assets/img/twitter-light.svg'
import '../assets/img/behance-black.svg'
import '../assets/img/facebook-black.svg'
import '../assets/img/twitter-black.svg'
import '../assets/img/twitter-grey.svg'
import '../assets/img/phone-grey.svg'
import '../assets/img/clock-grey.svg'
import '../assets/img/email-grey.svg'
import '../assets/img/behance-grey-fill.svg'
import '../assets/img/twitter-grey-fill.svg'
import '../assets/img/facebook-grey-fill.svg'
import '../assets/img/grey-arrow-next.svg'
import '../assets/img/grey-arrow-prev.svg'
import '../assets/img/landing.png'
import '../assets/img/personal.png'
import '../assets/img/coorp.png'
import '../assets/img/shop.png'
import '../assets/img/visit.png'
import '../assets/img/thanks.svg'



//Images
import '../assets/img/main-img.png'


// Main
import '../assets/img/logo.svg'