import $ from 'jquery'
import 'jquery-mask-plugin/dist/jquery.mask'
import IScroll from 'fullpage.js/vendors/scrolloverflow';
import fullpage from 'fullpage.js';
import Swiper from 'swiper/swiper-bundle'
import 'swiper/swiper-bundle.css';

$(function () {

    $('body').removeClass('loading')
    $('.loader').removeClass('showed')

    let full_page
    const fullpageOptions = {
        sectionSelector: '.page-section',
        scrollOverflow: true,
        anchors: ['main', 'projects', 'services', 'work', 'contacts'],
        navigation: true,
        afterLoad: function (origin, destination) {
            if (origin.anchor === 'main' && destination.anchor === 'main') {
                $('.header').addClass('show')
                $('.header').removeClass('filled')
            }
        },
        onLeave: function (origin, destination) {
            if (origin.anchor === 'main') {
                $('.header').removeClass('show')
            } else if (destination.anchor === 'main') {
                $('.header').addClass('show')
            }

            if (destination.anchor === 'main' || destination.anchor === 'contacts') {
                $('.header').removeClass('filled')
            } else if (origin.anchor === 'main' || origin.anchor === 'contacts') {
                $('.header').addClass('filled')
            }
        }
    }

    if (window.innerWidth > 800) {
        full_page = new fullpage('.full-page', fullpageOptions)
    }

    window.addEventListener('resize', function () {
        if (window.innerWidth <= 800) {
            if (!$('.full-page').hasClass('fp-destroyed') && full_page) {
                full_page.destroy('all')
            }
        } else {
            if ($('.full-page').hasClass('fp-destroyed') || !full_page) {
                full_page = new fullpage('.full-page', fullpageOptions)
            }
        }
    })

    checkMobileMenu()
    window.addEventListener('scroll', checkMobileMenu)

    const projects = new Swiper('.projects__inner', {
        freeMode: true,
        slidesPerView: 'auto',
        direction: "horizontal",
        spaceBetween: 20,
        grabCursor: true,
        watchOverflow: true,
        breakpoints: {
            1367: {
                spaceBetween: 40
            },
        }
    })


    let work = null
    const workOptions = {
        navigation: {
            prevEl: '.work__arrow_prev',
            nextEl: '.work__arrow_next',
        },
        pagination: {
            el: '.work__dots',
            clickable: true
        },
        breakpoints: {
            1051: {
                spaceBetween: 32,
                slidesPerView: 2,
                slidesPerGroup: 2,
                slidesPerColumn: 2,
                slidesPerColumnFill: 'row',
            },
            1367: {
                spaceBetween: 0,
                slidesPerView: 2,
                slidesPerGroup: 2,
                slidesPerColumn: 2,
                slidesPerColumnFill: 'row',
            }
        }
    }

    if (window.innerWidth > 800) {
        work = new Swiper('.work__slider', workOptions)
    }


    window.addEventListener('resize', function () {
        if (window.innerWidth <= 800) {
            if (work) {
                work.destroy(true, true)
                work = null
            }
        } else {
            if (!work) {
                work = new Swiper('.work__slider', workOptions)
            }
        }
    })

    const servicesOptions = {
        spaceBetween: 10,
        slidesPerColumn: 2,
        slidesPerView: 2,
        slidesPerGroup: 1,
        slidesPerColumnFill: 'row',
        watchOverflow: true,
        grabCursor: true,
        navigation: {
            prevEl: '.services__arrow_prev',
            nextEl: '.services__arrow_next',
        },
        pagination: {
            el: '.services__dots',
            clickable: true
        },
        breakpoints: {
            1367: {
                spaceBetween: 20
            },
            1441: {
                spaceBetween: 60,
            },
            1731: {
                slidesPerView: 3,
            }
        }
    }

    let services = null

    if (window.innerWidth > 1024) {
        services = new Swiper('.services__inner', servicesOptions)
    }


    window.addEventListener('resize', function () {
        if (window.innerWidth <= 1024) {
            if (services) {
                services.destroy(true, true)
                services = null
            }
        } else {
            if (!services) {
                services = new Swiper('.services__inner', servicesOptions)
            }
        }
    })

    let currentServices = null
    $('.services__item').on('click', function (event) {
        if (window.innerWidth > 1024) return

        const target = event.target.closest('.swiper-slide')
        const item = event.target.closest('.services__item')

        if (currentServices && currentServices !== target) {
            $(currentServices).removeClass('active')
            $(currentServices).find('.services__item').removeClass('_1')
            $(currentServices).find('.services__item').removeClass('_2')

            currentServices = target
            $(currentServices).addClass('active')
            $(currentServices).find('.services__item').addClass('_1')
        } else if (!currentServices) {
            currentServices = target
            $(currentServices).addClass('active')
            $(currentServices).find('.services__item').addClass('_1')
        } else {
            if ($(currentServices).find('.services__item').hasClass('_1')) {
                $(currentServices).find('.services__item').removeClass('_1')
                $(currentServices).find('.services__item').addClass('_2')
            } else if ($(currentServices).find('.services__item').hasClass('_2') && !event.target.closest('.services__info-btn')) {
                $(currentServices).removeClass('active')
                $(currentServices).find('.services__item').removeClass('_2')
                currentServices = null
            }
        }
    })


    $('.btn-modal').on('click', function () {
        openModal('form-module')
    })


    document.addEventListener('change', function () {
        checkInput(event.target);
    });

    document.addEventListener('submit', checkForm);

    let errs = {};
    document.querySelectorAll('form').forEach((form) => {
        errs[form.getAttribute('name')] = [];
    });

    $('.header__menu-btn').on('click', function () {
        if ($('.header__content').hasClass('show')) {
            $('.header__content').removeClass('show')
            document.removeEventListener('click', closeMenu)
        } else {
            $('.header__content').addClass('show')
            document.addEventListener('click', closeMenu)
        }
    })

    $('.header__content .close').on('click', function () {
        $('.header__content').removeClass('show')
        document.removeEventListener('click', closeMenu)
    })

    $('.navigation').on('click', '.navigation__link', function (event) {
        if (window.innerWidth > 800) return

        const target = event.target.dataset.target

        let top = 0

        if (target !== 'main') {
            const el = document.querySelector('.page-section.' + target)
            top = $(el).offset().top - $('.header').outerHeight()
        }

        if (event.target.closest('.header__content')) {
            $('.header__content').removeClass('show')
            document.removeEventListener('click', closeMenu)
        }

        $('html, body').animate({
            scrollTop: top
        }, 500);
    })


    // $('input[type="tel"]').each(function () {
    //     $(this).mask('+7 (000) 000 00 00', {
    //         onChange: function (cep, e) {
    //             checkPhone(cep, e);
    //         }
    //     });
    // });


    //FUNCTIONS

    // MENU

    function checkMobileMenu() {
        if (window.innerWidth > 800) return

        if (document.documentElement.scrollTop === 0) {
            $('.header').removeClass('filled')
        } else {
            $('.header').addClass('filled')
        }
    }

    function closeMenu(event) {
        if (event.target.closest('.header__content') || event.target.closest('.header__menu-btn')) return

        $('.header__content').removeClass('show')
        document.removeEventListener('click', closeMenu)
    }


    // MODAL

    function openModal(module, event) {
        const modal = document.querySelector('.modal')
        const moduleHTML = document.querySelector('.' + module).innerHTML

        modal.querySelector('.modal__body').innerHTML = moduleHTML

        $(modal).find('input[type="tel"]').each(function () {
            $(this).mask('+7 (000) 000 00 00', {
                onChange: function (cep, e) {
                    checkPhone(cep, e);
                }
            });
        });

        modal.style.display = 'flex'
        setTimeout(() => {
            modal.classList.add('show')
        }, 0)

        document.addEventListener('click', closeModal)
        document.addEventListener('keyup', closeModal)
    }

    function closeModal(event) {
        if ((event.target && event.target.closest('.close')) || (event.target && event.target.classList.contains('modal')) || event.code === 'Escape') {
            const modal = document.querySelector('.modal')


            modal.classList.remove('show')

            setTimeout(() => {
                modal.querySelector('.modal__inner').classList.remove('modal__inner_thanks')
                modal.style.display = ''
            }, 300)
        }
    }


    // FORM

    function checkInput(target) {
        let type = target.type;
        let form = target.closest('form');
        const formName = form.getAttribute('name')
        let regexp;
        let index = errs[formName].indexOf(target);

        switch (type) {
            case 'text':
                if (target.dataset.onlywords) regexp = /^[a-zA-Zа-яА-Я][a-zA-Zа-яА-Я\s]+$/;
                else regexp = /^.+$/

                if (!target.value) {
                    form[target.name + 'Valid'] = false;
                    target.closest('.input-wrapper').classList.remove('error');
                    if (index != -1) {
                        errs[formName].splice(index, 1);
                    }
                } else if (regexp.test(target.value)) {
                    form[target.name + 'Valid'] = true;
                    target.closest('.input-wrapper').classList.remove('error');
                    if (index != -1) {
                        errs[formName].splice(index, 1);
                    }
                } else {
                    form[target.name + 'Valid'] = false;
                    target.closest('.input-wrapper').classList.add('error');
                    if (!errs[formName].includes(target)) {
                        errs[formName].push(target);
                    }
                }
                break;
            case 'email':
                regexp = /^[\w-.]+@([-\w]+\.)+[-\w]+$/;

                if (!target.value) {
                    form.emailValid = false;
                    target.closest('.input-wrapper').classList.remove('error');
                    if (index != -1) {
                        errs[formName].splice(index, 1);
                    }
                } else if (regexp.test(target.value)) {
                    form.emailValid = true;
                    target.closest('.input-wrapper').classList.remove('error');
                    if (index != -1) {
                        errs[formName].splice(index, 1);
                    }
                } else {
                    form.emailValid = false;
                    target.closest('.input-wrapper').classList.add('error');
                    if (!errs[formName].includes(target)) {
                        errs[formName].push(target);
                    }
                }
                break;
            case 'textarea':
                if (!target.value) {
                    form.messageValid = false;
                    target.closest('.input-wrapper').classList.remove('error');
                    if (index != -1) {
                        errs[formName].splice(index, 1);
                    }
                } else if (target.value.length > 5) {
                    form.messageValid = true;
                    target.closest('.input-wrapper').classList.remove('error');
                    if (index != -1) {
                        errs[formName].splice(index, 1);
                    }
                } else {
                    form.messageValid = false;
                    target.closest('.input-wrapper').classList.add('error');
                    if (!errs[formName].includes(target)) {
                        errs[formName].push(target);
                    }
                }
                break;
        }
    }

    function checkPhone(value, event) {
        let target = event.target;
        let form = target.closest('form');
        const formName = form.getAttribute('name')
        let index = errs[formName].indexOf(target);

        if (target.value.length == 0) {
            form.phoneValid = false;
            target.closest('.input-wrapper').classList.remove('error');
            if (index != -1) {
                errs[formName].splice(index, 1);
            }
        } else if (target.value.length == 18) {
            form.phoneValid = true;
            target.closest('.input-wrapper').classList.remove('error');
            if (index != -1) {
                errs[formName].splice(index, 1);
            }
        } else {
            form.phoneValid = false;
            target.closest('.input-wrapper').classList.add('error');
            if (!errs[formName].includes(target)) {
                errs[formName].push(target);
            }
        }
    }

    function checkForm() {
        event.preventDefault();

        let form = event.target;
        const formName = form.getAttribute('name')
        errs[formName] = []

        let data = new FormData();
        if (form.name) {
            if (!(form.nameValid) && !(errs[formName].includes(form.name))) {
                errs[formName].push(form.name);
            } else {
                data.append('name', form.name.value);
            }
        }
        if (form.phone) {
            if (!(form.phoneValid) && !(errs[formName].includes(form.phone))) {
                errs[formName].push(form.phone);
            } else {
                data.append('phone', form.phone.value);
            }
        }
        if (form.message) {
            if (!(form.messageValid) && !(errs[formName].includes(form.message))) {
                errs[formName].push(form.message);
            } else {
                data.append('message', form.message.value);
            }
        }
        if (form.email) {
            if (!(form.emailValid) && !(errs[formName].includes(form.email))) {
                errs[formName].push(form.email);
            } else {
                data.append('email', form.email.value);
            }
        }

        if (errs[formName].length != 0) {
            for (let err of errs[formName]) {
                err.closest('.input-wrapper').classList.add('error');
            }
            return;
        }

        let modalBody = form.closest('.modal__body');
        modalBody.classList.add('off');

        setTimeout(() => {
            modalBody.innerHTML = document.querySelector('.thanks-module').innerHTML;
            modalBody.closest('.modal__inner').classList.add('modal__inner_thanks');
            setTimeout(() => {
                modalBody.classList.remove('off');
            }, 200);
        }, 300);
    }

    function clearForm(form) {
        if (form.name) {
            form.name.value = '';
            form.nameValid = false;
        }
        if (form.email) {
            form.email.value = '';
            form.emailValid = false;
        }
        if (form.phone) {
            form.phone.value = '';
            form.phoneValid = false;
        }
    }


})

